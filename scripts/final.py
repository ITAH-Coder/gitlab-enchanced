#!/usr/bin/env python
import os, subprocess, sys, time, json, requests, boto3

#################################
### Preliminary actions
#################################

# Main Variables
print "-------------------"
PROJECT_NAME = sys.argv[1]
ENV = sys.argv[2]
TEMPL = sys.argv[3]
api_token = os.environ.get('API_TOKEN')
myUrl = os.environ.get('GITURL')
headers = {'Content-Type': 'application/json',
           'Accept':'application/json',
           'Authorization': 'Bearer {0}'.format(api_token)}

# Getting Root ID
r_id = requests.get('{}groups?search=devops'.format(myUrl), headers=headers)
#print r_id.status_code
r_data = (r_id.json())
root_id =  (r_data[0]['id'])
print "Getting necessary information:"
print "Root ID:", root_id
#
# Depends on ENV variable set definition of:
if ENV =='Full':
    branch_list = ["develop","uat","pp","prod"]
    table_list = ["develop","uat","pp","prod"]
else:
    branch_list = ["develop","prod"]
    table_list = ["develop","prod"]
###

print "-------------------"
print "PN:", PROJECT_NAME
print "Environment:", ENV

##########  END BLOCK ############

#################################
###   Creating New Project
#################################
print "-------------------"
# -> Data to be send to API
data = {'path':PROJECT_NAME,
        'namespace_id':root_id}

project_id = requests.get('{}groups/{}/projects?search={}'.format(myUrl, root_id, PROJECT_NAME), headers=headers)
#print project_id.status_code
p_data = (project_id.json())
#print p_data
# Checking if Project exsist or not
# if len(p_data) == 0:
print("Creating Project...")
project_r = requests.post('{}projects'.format(myUrl), data=json.dumps(data), headers=headers )
time.sleep(2)
#Get ProjectID after build
p_id = requests.get('{}groups/{}/projects?search={}'.format(myUrl, root_id, PROJECT_NAME), headers=headers)
new_data = (p_id.json())
PROJECT_ID = (new_data[0]['id'])
print "ProjectID:", PROJECT_ID
##########  END BLOCK ############


#################################
###   Adding Variables to CI/CD
#################################
print "-------------------"
print "Adding variables to new projects..."
list_var = ["ANSIBLE_HOST_KEY_CHECKING", "ANSIBLE_SSH_PRV", "ANSIBLE_SSH_PUB", \
            "AWS_ACCESS_KEY_ID", "AWS_SECRET_ACCESS_KEY", "GIT_PASS", "GIT_USER", "TF_IN_AUTOMATION"]
for git_var in list_var:
    variable_data = {
        "key": git_var,
        "value": os.environ.get(git_var),
        "protected": 'flase',
        "variable_type": "env_var",
        "protected": 'false',
        "masked": 'false',
        "environment_scope": "*",
        }
    #print json.dumps(variable_data)
    variable_r = requests.post('{}projects/{}/variables'.format(myUrl, PROJECT_ID), data=json.dumps(variable_data), headers=headers)
    #print variable_r.content
    print "Status code of POST variables:", variable_r.status_code
##########  END BLOCK ############


#################################
###   Assign Runners to Project
#################################
print "-------------------"
print "Assign existing Runners to New Project"
# Getting ID of Runners
runners_all = requests.get('{}runners/all'.format(myUrl), headers=headers)
run_data = (runners_all.json())
#print json.dumps(run_data, indent=4)

# Finding correct Runners and assiggn its ID to variable list
substring = "gitlab-runner-in-admin-production-eu-central"
runner_correct_list=[]
for item in run_data:
    if substring in item['description']:
        #print item['id']
        runner_correct_list.append(item['id'])
print "Runners ID:", json.dumps(runner_correct_list)

# Assign corrent runner to project
for runner in runner_correct_list:
    runner_data = {
        "runner_id": runner
        }
    runner_r = requests.post('{}projects/{}/runners'.format(myUrl, PROJECT_ID), data=json.dumps(runner_data), headers=headers)
    print "Status code of POST runner:", runner_r.status_code
# ##########  END BLOCK ############


#################################
###   BRANCHES
#################################
print "-------------------"
# Creating branches develop & prod
# Variable branch_list is defined at the begining of script
for branch_name in branch_list:
    branch_r = requests.post('{}projects/{}/repository/branches?branch={}&ref=master'.format(myUrl, PROJECT_ID, branch_name), headers=headers)

# Unprotect master branch
branch_unp = requests.delete('{}projects/{}/protected_branches/master'.format(myUrl, PROJECT_ID), headers=headers)
#print branch_unp.content

# Set branch prod as default
branch_p = requests.put('{}projects/{}?default_branch=prod'.format(myUrl, PROJECT_ID), headers=headers)
#print branch_p.content

# Delete master branch
branch_del = requests.delete('{}projects/{}/repository/branches/master'.format(myUrl, PROJECT_ID), headers=headers)
#print branch_del.content

# Protect master branch
branch_pro = requests.post('{}projects/{}/protected_branches?name=prod&push_access_level=30&merge_access_level=30&unprotect_access_level=40'.format(myUrl, PROJECT_ID), headers=headers)
#print branch_pro.content
print "Creating branches:"
print "Status Code of DELETE, PUT, POST Branches", branch_unp.status_code, branch_pro.status_code, branch_p.status_code, branch_pro.status_code
# ##########  END BLOCK ############


#################################
###   Slack notification
#################################
print "-------------------"
print "SLack: "
slack_data = {
    "webhook": os.environ.get('WEBHOOK'),
    "username": "GitLab"
}

slack_r = requests.put('{}projects/{}/services/slack'.format(myUrl, PROJECT_ID), data=json.dumps(slack_data), headers=headers)
#print "Slack Notification:", slack_r.content
print "Status code of POST Slack:", slack_r.status_code


#################################
###   DynamoDB
#################################
print "-------------------"
print "Creating DynamoDB"
session = boto3.Session(
        aws_access_key_id=os.environ.get('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=os.environ.get('AWS_SECRET_ACCESS_KEY')
        )
dynamodb = boto3.resource('dynamodb', region_name='eu-central-1')

# Variable table_list is defined at the begining of script
for env_table in table_list:
    table = dynamodb.create_table(
        TableName='{}_terraform_{}_remote_state_lock'.format(PROJECT_NAME, env_table),
        KeySchema=[
            {
                'AttributeName': 'LockID',
                'KeyType': 'HASH'  #Partition key
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'LockID',
                'AttributeType': 'S'
            }
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 10,
            'WriteCapacityUnits': 10,
            }
    )
    print("Table status:", table.table_status)
##########  END BLOCK ############


#################################
###   S3 Bucket
#################################
print "-------------------"
print "Creating S3 Bucket"
session = boto3.Session(
        aws_access_key_id=os.environ.get('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=os.environ.get('AWS_SECRET_ACCESS_KEY')
        )
# Changing any uppercase on lowercase
s3_name = PROJECT_NAME.lower()
# Bucket name definition
bucket_name = 'iac-{}-remote-state'.format(s3_name)
s3_client = boto3.client('s3', region_name='eu-central-1')
location = {'LocationConstraint': 'eu-central-1'}
s3_client.create_bucket(Bucket=bucket_name, CreateBucketConfiguration=location)
##########  END BLOCK ############


######### TEMPLATE  ##############
#
# Main Condition - Template yes/no
if TEMPL =='Yes':
    print "Template - YES"

    ##### Replace string in Make FIle
    FILE_LIST = ["Make", "Backend"]
    for F_NAME in FILE_LIST:
        fin = open('/var/opt/gitlabapi-flask/apiproject/scripts/Repo/pattern_{}'.format(F_NAME), "rt")
        fout = open('/var/opt/gitlabapi-flask/apiproject/scripts/Repo/{}_file'.format(F_NAME), "wt")
        for line in fin:
            fout.write(line.replace('XXX', PROJECT_NAME))
        fin.close()
        fout.close()
    #### END #########

    ####### REPO FILES ############
    with open('/var/opt/gitlabapi-flask/apiproject/scripts/Repo/Make_file', 'r') as myfile:
        make_file = myfile.read()

    with open('/var/opt/gitlabapi-flask/apiproject/scripts/Repo/.gitlab-ci.yml', 'r') as myfile:
        gitlab_file = myfile.read()
    
    # Terraform files:
    # tfvars
    with open('/var/opt/gitlabapi-flask/apiproject/scripts/Repo/uat.tfvars', 'r') as myfile:
        uat_file = myfile.read()

    with open('/var/opt/gitlabapi-flask/apiproject/scripts/Repo/pp.tfvars', 'r') as myfile:
        pp_file = myfile.read()

    with open('/var/opt/gitlabapi-flask/apiproject/scripts/Repo/prod.tfvars', 'r') as myfile:
        prod_file = myfile.read()

    with open('/var/opt/gitlabapi-flask/apiproject/scripts/Repo/authorized_keys.sh.tpl', 'r') as myfile:
        keysh_file = myfile.read()

   
    with open('/var/opt/gitlabapi-flask/apiproject/scripts/Repo/Backend_file', 'r') as myfile:
        backend_file = myfile.read()

    with open('/var/opt/gitlabapi-flask/apiproject/scripts/Repo/policy.tf', 'r') as myfile:
        policy_file = myfile.read()

    with open('/var/opt/gitlabapi-flask/apiproject/scripts/Repo/provider.tf', 'r') as myfile:
        provider_file = myfile.read()

    with open('/var/opt/gitlabapi-flask/apiproject/scripts/Repo/variables.tf', 'r') as myfile:
        var_file = myfile.read()

    with open('/var/opt/gitlabapi-flask/apiproject/scripts/Repo/main.tf', 'r') as myfile:
        main_file = myfile.read()
    #
    #
    #
    variable_data = {
    "branch": "develop",
    "commit_message": "some commit message",
    "actions": [
        {
        "action": "create",
        "file_path": "Makefile",
        "content": make_file
        },
        {
        "action": "create",
        "file_path": ".gitlab-ci.yml",
        "content": gitlab_file
        },
        {
        "action": "create",
        "file_path": "/terraform/environment/uat/infrastructure.tfvars",
        "content": uat_file
        },
        {
        "action": "create",
        "file_path": "/terraform/environment/pp/infrastructure.tfvars",
        "content": pp_file
        },
        {
        "action": "create",
        "file_path": "/terraform/environment/prod/infrastructure.tfvars",
        "content": prod_file
        },
        {
        "action": "create",
        "file_path": "/terraform/templates/authorized_keys.sh.tpl",
        "content": keysh_file
        },
        {
        "action": "create",
        "file_path": "/terraform/backend.tf",
        "content": backend_file
        },
        {
        "action": "create",
        "file_path": "/terraform/policy.tf",
        "content": policy_file
        },
        {
        "action": "create",
        "file_path": "/terraform/provider.tf",
        "content": provider_file
        },
        {
        "action": "create",
        "file_path": "/terraform/variables.tf",
        "content": var_file
        },
        {
        "action": "create",
        "file_path": "/terraform/main.tf",
        "content": main_file
        },
    ]
    }

    variable_r = requests.post('{}projects/{}/repository/commits'.format(myUrl, PROJECT_ID), data=json.dumps(variable_data), headers=headers)
    #print variable_r.content
    print "Status code of POST variables:", variable_r.status_code

else:
    print "Template - NO"

### END of TEMAPLE Block

print "Process has been made."
