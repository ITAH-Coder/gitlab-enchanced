# Additional frontened for GitLab. Enable build new project with automatic creation of all necessary configuration activities.
Python+Flask+GitLab API+UWSGI


**Project Goal:**

Build an extension allowing the regular version of GitLab to have the properties of the Enterprise.
This is exactly about building a New Project in GiTLAB. This python app enable customized template that will automatically add and configure all necessary parameters such as:
- Variables CI / CD
- Runners
- Branches
- Slack notification
- DynamoDB
- S3 Bucke

Frontend:
 - Flask
 - Python
 - HTML

Backend:
 - python
 - GitLab API
 
